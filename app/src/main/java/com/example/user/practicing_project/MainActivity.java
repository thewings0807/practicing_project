package com.example.user.practicing_project;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int REQUEST_PICK = 0;
    public static final int REQUEST_CAMERA = 1;
    public static final int ALL_PERMISSION_REQUEST_CODE = 10;
    public static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 11;
    public static final int CAMERA_REQUEST_CODE = 12;
    private static final String TAG = "MainActivity";
    private ImageView img;
    private File outputFile;
    private RequestQueue requestQueue;
    private AlertDialog dialog;
    private TextView tv_result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("天氣辨識小幫手");
        img = findViewById(R.id.img);
        SSLTrustManager.allowAllSSL();
        requestQueue = Volley.newRequestQueue(this);
        tv_result = findViewById(R.id.tv_result);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_upload:
                if (checkPermission()) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("請問要去相機還是相簿呢?")
                            .setItems(new String[]{"相機", "相簿"}, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case 0:
                                            gotoCamera();
                                            break;
                                        case 1:
                                            pickImage();
                                            break;
                                    }
                                }
                            }).show();
                }
                break;
            case R.id.bt_identify:
                //上傳照片
                Bitmap bmp = ((BitmapDrawable) img.getDrawable()).getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                Map<String, VolleyMultipartRequest.DataPart> image_body = new HashMap<>();
                image_body.put("image_file", new VolleyMultipartRequest.DataPart("file.jpg", stream.toByteArray(), "image/jpeg"));
                Map<String, String> body = new HashMap<>();
                body.put("job_id", "20180805-125241-f40c");
                VolleyMultipartRequest request = new VolleyMultipartRequest(Request.Method.POST, "http://87d860c6.ngrok.io/models/images/classification/classify_one.json", body, image_body, new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        dialog.dismiss();
                        try {
                            if (response.data.length > 0) {
                                String jsonString = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                Log.d(TAG, "onResponse: " + new JSONObject(jsonString));
                                JSONObject text = new JSONObject(jsonString);
                                JSONArray array = text.getJSONArray("predictions");
                                String result = "";
                                for (int i = 0; i < array.length(); i++)
                                {
                                    JSONArray arr = array.getJSONArray(i);
                                    result += arr.getString(0)  + " : " + arr.getDouble(1) + "% \n";
                                }
                                tv_result.setText(result);
                            } else {
                                Log.d(TAG, "parseNetworkResponse: response no data");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "parseNetworkResponse: success but error " + e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        try {
                            if (error.networkResponse != null) {
                                String jsonString = new String(error.networkResponse.data,
                                        HttpHeaderParser.parseCharset(error.networkResponse.headers, "utf-8"));
                                Log.e(TAG, "onErrorResponse: " + new JSONObject(jsonString));
                            }
                            Log.e(TAG, "onErrorResponse: " + error.toString());
                        } catch (Exception e) {
                            Log.e(TAG, "onErrorResponse: " + e.getMessage());
                        }
                    }
                });
                request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(request);
                dialog = new AlertDialog.Builder(this, R.style.TransparentDialog)
                        .setCancelable(false)
                        .setView(R.layout.dialog_wait_for_login)
                        .show();
                break;
        }
    }

    private boolean checkPermission() {
        boolean b = false;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                    ALL_PERMISSION_REQUEST_CODE);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    CAMERA_REQUEST_CODE);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
        } else {
            b = true;
        }
        return b;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ALL_PERMISSION_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                } else {
                    // Permission Denied
                }
                break;
            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                } else {
                    // Permission Denied
                }
                break;
            case CAMERA_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                } else {
                    // Permission Denied
                }
                break;
        }
    }

    private void gotoCamera() {
        String filepath = Environment.getExternalStorageDirectory() + "/images/" + System.currentTimeMillis() + ".jpg";
        outputFile = new File(filepath);
        if (!outputFile.getParentFile().exists()) {
            outputFile.getParentFile().mkdir();
        }
        Uri contentUri = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".fileprovider", outputFile);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, null);
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        intent = Intent.createChooser(intent, "請選擇圖片來源");
        startActivityForResult(intent, REQUEST_PICK);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PICK:
                    try {
                        Bitmap b = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                        img.setImageBitmap(b);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case REQUEST_CAMERA:
                    try {
                        Bitmap b = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(outputFile));
                        img.setImageBitmap(b);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
